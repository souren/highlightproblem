//: A Cocoa based Playground to present user interface

import AppKit
import Cocoa
import PlaygroundSupport

protocol ButtonColorMapProtocol
{
    var buttonBorderColor: NSColor { get }
    var buttonBackgroundColor: NSColor { get }
    var buttonBackgroundDisabledColor: NSColor { get }
    var buttonHighlightColor: NSColor { get }
    var buttonOnColor: NSColor { get }
    var buttonLabelColor: NSColor { get }
}

protocol KeypadConfigurationProtocol
{
    var borderWidth: CGFloat { get }
    var buttonWidth: CGFloat { get }
    var buttonHeight:CGFloat { get }
    var cornerRadius: CGFloat { get }
    var buttonInset: CGFloat { get }
    var padding: CGFloat { get }
    var fontSize: CGFloat { get }
}

protocol ButtonActionDelegate: class
{
    func buttonPressed(btn: CKey)
}

enum CKey
{
    case zero
}

struct ButtonColorMapDefault : ButtonColorMapProtocol
{
    var buttonBorderColor = NSColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
    var buttonBackgroundColor = NSColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1.0)
    var buttonBackgroundDisabledColor = NSColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 0.8)
    var buttonHighlightColor = NSColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0)
    var buttonOnColor = NSColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)
    var buttonLabelColor = NSColor(red:0.125, green:0.125, blue:0.125, alpha:1.000)
}

struct NumKeypadConstants : KeypadConfigurationProtocol
{
    var borderWidth: CGFloat = 2.0
    var buttonWidth: CGFloat = 70.0
    var buttonHeight: CGFloat = 40.0
    var cornerRadius: CGFloat = 25.0
    var buttonInset: CGFloat = 1.0
    var padding: CGFloat = -2.0
    var fontSize: CGFloat = 50.0
}



class FunkyCell: NSButtonCell
{
    internal var config: KeypadConfigurationProtocol
    internal var colorMap: ButtonColorMapProtocol
    
    init(textCell string: String, withColorMap colorMap: ButtonColorMapProtocol, withConfig configuration: KeypadConfigurationProtocol)
    {
        config = configuration
        self.colorMap = colorMap
        super.init(textCell: string)
        
        let atrString = NSMutableAttributedString()
        
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        
        atrString.append(NSAttributedString(string: string, attributes:
            [
                .foregroundColor: colorMap.buttonLabelColor,
                .paragraphStyle: style,
                .font: NSFont.boldSystemFont(ofSize: CGFloat(configuration.fontSize))
            ]))
        
        self.attributedTitle = atrString
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func redraw() {
        self.backgroundColor = self.backgroundColor
    }
    
    override func drawBezel(withFrame frame: NSRect, in controlView: NSView) {
        let path = NSBezierPath(roundedRect: frame.insetBy(dx: CGFloat(config.buttonInset), dy: CGFloat(config.buttonInset)), xRadius: config.cornerRadius, yRadius: config.cornerRadius)
        path.lineWidth = config.borderWidth
        if(isEnabled)
        {
            if(isHighlighted)
            {
                print("isHighlighted true")
                let fillColor: NSColor = colorMap.buttonHighlightColor
                let strokeColor: NSColor = colorMap.buttonBorderColor
                fillColor.setFill()
                strokeColor.setStroke()
                path.fill()
                path.stroke()
            }
            else
            {
                print("isHighlighted false")
                if(showsStateBy.contains(.changeGrayCellMask))
                {
                    print(".changeGrayCellMask")
                    if(state == .on)
                    {
                        print(".on")
                        let fillColor: NSColor = colorMap.buttonOnColor
                        let strokeColor: NSColor = colorMap.buttonBorderColor
                        fillColor.setFill()
                        strokeColor.setStroke()
                        path.fill()
                        path.stroke()
                    }
                    else
                    {
                        print(".off")
                        let fillColor: NSColor = colorMap.buttonBackgroundColor
                        let strokeColor: NSColor = colorMap.buttonBorderColor
                        fillColor.setFill()
                        strokeColor.setStroke()
                        path.fill()
                        path.stroke()
                    }
                }
                else
                {
                    print("!.changeGrayCellMask")
                    let fillColor: NSColor = colorMap.buttonBackgroundColor
                    let strokeColor: NSColor = colorMap.buttonBorderColor
                    fillColor.setFill()
                    strokeColor.setStroke()
                    path.fill()
                    path.stroke()
                }
            }
        }
        else
        {
            let fillColor: NSColor = colorMap.buttonBackgroundDisabledColor
            let strokeColor: NSColor = colorMap.buttonBorderColor
            fillColor.setFill()
            strokeColor.setStroke()
            path.fill()
            path.stroke()
        }
    }
}

class CalcButton: NSButton
{
    weak var delegate: ButtonActionDelegate?
    var buttonIdentifier: CKey = .zero
    
    convenience init(frame frameRect: NSRect, buttonType: NSButton.ButtonType, withLabel label: String, withColorMap colorMap: ButtonColorMapProtocol, withConfig configuration: KeypadConfigurationProtocol) {
        self.init(frame: frameRect)
        
        cell = FunkyCell(textCell: label, withColorMap:colorMap, withConfig: configuration)
        if let c = cell as? FunkyCell
        {
            c.state = .off
        }
        
        setButtonType(buttonType)
        bezelStyle = .smallSquare
        
        target = self
        action = #selector(self.react(_:))
    }
    
    @objc func react(_ sender: NSButton)
    {
        delegate?.buttonPressed(btn: buttonIdentifier)
    }
    
    required override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

let button = CalcButton(
    frame: NSMakeRect(0, 0, 100, 100),
    buttonType: .momentaryChange,
    withLabel: "5",
    withColorMap: ButtonColorMapDefault(),
    withConfig: NumKeypadConstants())

button.keyEquivalent = "5"

// Present the view in Playground
PlaygroundPage.current.liveView = button

